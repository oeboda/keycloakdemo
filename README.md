# KeycloakDemo #

This is a simple JAVA project to demostrate how to integrate Keycloak into a JAVA application for user identity management, user authentication and authorization.

Keycloak is an open source single sign on solution for centralized identity and access management (IAM).

Keycloak supports both OpenID Connect (an extension to OAuth 2.0) and SAML 2.0. 

[Learn more about keycloak](https://www.keycloak.org/docs/latest/server_admin/index.html)


 

### Project Summary ###

Java classes

* KeyCloakRealmManager.java - Shows how to initiate access to a keycloak realm with a service client and a user account

* KeyCloakClient.java - Shows how to perform various user management operations in keycloak from a java application. The demonstrated operations include 
    *  Add new user to keycloak
    *  Get user details from keycloak
    *  Update user details in keycloak
    *  User logon to keycloak using openID Connection protocol
    *  Reset user password

* User.java -  Keycloak User model.

* KeyCloakSampleOperations.java (Main) -  Initializes a sample keycloak realm and performs keycloak user operations with a sample user model.





### How do I get set up? ###

* Clone the project to a local repo
* Run command 'mvn clean package' from the project root folder to build the project
* Update keycloak configurations in file service_config.properties
* Execute class com.baobab.services.KeyCloakSampleOperations to run the sample user operations. 
	* (mvn compile exec:java -Dexec.mainClass="com.baobab.services.KeyCloakSampleOperations")

### Keycloak API Documentation ###
* [JavaDoc](https://www.keycloak.org/docs-api/12.0/javadocs/index.html) - Documentation for Java API
* [Administration REST API](https://www.keycloak.org/docs-api/12.0/rest-api/index.html) -Documentation for the Adminstration RESTful API
