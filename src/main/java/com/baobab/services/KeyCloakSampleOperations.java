/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services;

import com.baobab.services.iam.keycloak.JWTUtils;
import com.baobab.services.iam.keycloak.KeyCloakClient;
import com.baobab.services.iam.keycloak.KeyCloakRealmManager;
import com.baobab.services.iam.keycloak.KeyCloakOperationResult;
import com.baobab.services.model.User;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.JsonWebToken;

/**
 *
 * @author WARI
 */
public class KeyCloakSampleOperations {

    public static void main(String... args) {

        KeyCloakRealmManager.initialize();

        KeyCloakClient keyCloak = new KeyCloakClient();

        //Operation 1: Add new user to keycloak
        User user = new User();
        user.setFirstName("Lekan");
        user.setLastName("Eboda");
        user.setPhoneNumber("780175435");
        user.setEmailAddress("oeboda@baobabgroup.com");
        user.setPassword("176587");
        user.setUserName("oeboda");
        user.setCountryCode("SN");
        user.setBirthDate("20-02-1990");

        KeyCloakOperationResult kcOpResult = keyCloak.addUser(user);

        System.out.println(kcOpResult.getMessage());

        //Operation 2: Get user details from keycloak
        kcOpResult = keyCloak.getUser(user.getUserName());
        if (kcOpResult == KeyCloakOperationResult.Success) {
            User user2 = (User) kcOpResult.getData();
            user.setUserID(user2.getUserID());
            System.out.println(user2);
        }

        //Operation 3: Update user details in keycloak
        user.setFirstName("Olamilekan");
        kcOpResult = keyCloak.updateUser(user);
        if (kcOpResult == KeyCloakOperationResult.Success) {
            kcOpResult = keyCloak.getUser(user.getUserName());
            User user2 = (User) kcOpResult.getData();
            System.out.println(user2);
        } else {
            System.out.println(kcOpResult.getMessage());
        }

        //Operation 4: User logon to keycloak          
        kcOpResult = keyCloak.authenticateUser(user);

        if (kcOpResult == KeyCloakOperationResult.Success) {
            TokenManager tkMng = (TokenManager) kcOpResult.getData();
            AccessTokenResponse kcToken = tkMng.getAccessToken();
            System.out.println(kcToken.getTokenType() + " = " + kcToken.getToken() + " " + kcToken.getSessionState());
            
            try{
                JWTUtils.verifyToken(kcToken.getToken());
            }
            catch(ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException e){
                System.err.println("Token verification failed");
            }            

        } else {
            System.out.println(kcOpResult.getMessage());
        }

        //Operation 5: Reset user password
        String oldPassword = user.getPassword();
        user.setPassword("176588");
        kcOpResult = keyCloak.resetUserPassword(user);
        
        if (kcOpResult == KeyCloakOperationResult.Success) {
            user.setPassword(oldPassword);
            kcOpResult = keyCloak.authenticateUser(user);
            System.out.println(kcOpResult.getMessage());
        } 
        else {
            System.out.println("Password reset failed : " + kcOpResult.getMessage());
        }

    }

}
