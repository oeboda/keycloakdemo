/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.iam.keycloak;

/**
 *
 * @author WARI
 */
public enum KeyCloakOperationResult {

    Success(0, "Operation successfull", null),
    Failure(10000, "Operation failed", null),
    ErrorUserNotFound(10001, "User does not exists", null),
    ErrorUserAuthenticationFailed(10002, "User authentication failed", null),
    ErrorSystemException(10041, "Unspecified system error occured", null);

    private final int code;
    private String message;
    private Object data;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public KeyCloakOperationResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public KeyCloakOperationResult setData(Object data) {
        this.data = data;
        return this;
    }

    private KeyCloakOperationResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
