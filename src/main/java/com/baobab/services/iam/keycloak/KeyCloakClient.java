/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.iam.keycloak;

import com.baobab.services.model.User;
import static com.baobab.services.util.LogHandler.LogError;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

/**
 *
 * @author WARI
 */
public class KeyCloakClient {

    //call keycloak to create a new user profile
    public KeyCloakOperationResult addUser(User user) {
        UsersResource usersResource = KeyCloakRealmManager.getClientRealm().users();
        CredentialRepresentation credentialRepresentation = createPasswordCredentials(user.getPassword());

        UserRepresentation kcUser = new UserRepresentation();
        kcUser.setUsername(user.getUserName());
        kcUser.setCredentials(Collections.singletonList(credentialRepresentation));
        kcUser.setFirstName(user.getFirstName());
        kcUser.setLastName(user.getLastName());
        kcUser.setEmail(user.getEmailAddress());
        kcUser.singleAttribute("phone", user.getPhoneNumber());
        kcUser.singleAttribute("countrycode", user.getCountryCode());
        kcUser.singleAttribute("birthdate", user.getBirthDate());
        kcUser.setEnabled(true);
        kcUser.setEmailVerified(false);
//        Response r = usersResource.create(kcUser);
        try {
            Response r = usersResource.create(kcUser);
            
            if (r.getStatus() == 201) {
                return KeyCloakOperationResult.Success;
            } else {
                String keyCloakResponse = r.readEntity(String.class);
                JSONObject json = new JSONObject(keyCloakResponse);
                //System.out.println("KeyCloak User Create Response " + keyCloakResponse);
                if (json.has("errorMessage")) {
                    return KeyCloakOperationResult.Failure.setMessage(json.getString("errorMessage"));
                } else {
                    return KeyCloakOperationResult.Failure;
                }
            }
        } catch (Exception e) {
            LogError("Error occured while adding new keycloak user ", e);
            return KeyCloakOperationResult.ErrorSystemException.setMessage(e.getMessage());
        }

    }

    //call keycloak to get details of an existing user
    public KeyCloakOperationResult getUser(String userName) {

        try {
            List<UserRepresentation> users = KeyCloakRealmManager.getClientRealm().users().search(userName);
            if (users.size() > 0) {
                UserRepresentation kcUser = users.get(0);
                User user = new User();
                user.setBirthDate(kcUser.firstAttribute("birthdate"));
                user.setCountryCode(kcUser.firstAttribute("countrycode"));
                user.setPhoneNumber(kcUser.firstAttribute("phone"));
                user.setEmailAddress(kcUser.getEmail());
                user.setFirstName(kcUser.getFirstName());
                user.setLastName(kcUser.getLastName());
                user.setUserName(kcUser.getUsername());
                user.setUserID(kcUser.getId());
                return KeyCloakOperationResult.Success.setData(user);
            } else {
                return KeyCloakOperationResult.ErrorUserNotFound;

            }
        } catch (Exception e) {
            LogError("Error occured during keycloak user update", e);
            return KeyCloakOperationResult.ErrorSystemException.setMessage(e.getMessage());
        }

    }

    //call keycloak to update an existing user profile
    public KeyCloakOperationResult updateUser(User user) {

        try {
            UserResource userResource = getUserResource(user);
            UserRepresentation kcUser = userResource.toRepresentation();
            kcUser.setFirstName(user.getFirstName());
            kcUser.setLastName(user.getLastName());
            kcUser.setEmail(user.getEmailAddress());
            kcUser.singleAttribute("phone", user.getPhoneNumber());
            kcUser.singleAttribute("countrycode", user.getCountryCode());
            kcUser.singleAttribute("birthdate", user.getBirthDate());
            userResource.update(kcUser);
            return KeyCloakOperationResult.Success;
        } catch (javax.ws.rs.NotFoundException e) {
            LogError("Error occured during keycloak user update", e);
            return KeyCloakOperationResult.ErrorUserNotFound;
        } catch (Exception e) {
            LogError("Error occured during keycloak user update", e);
            return KeyCloakOperationResult.ErrorSystemException.setMessage(e.getMessage());
        }

    }

    //logon to keycloak with a user profile
    public KeyCloakOperationResult authenticateUser(User user) {
        try {
           
            TokenManager tkMng = KeyCloakRealmManager.getTokenManager(user.getUserName(),user.getPassword());
            tkMng.grantToken();
            return KeyCloakOperationResult.Success.setData(tkMng);

        } catch (javax.ws.rs.NotAuthorizedException e) {
            return KeyCloakOperationResult.ErrorUserAuthenticationFailed;
        } catch (Exception e) {
            LogError("Error occured while authenticating keycloak user ", e);
            return KeyCloakOperationResult.ErrorSystemException.setMessage(e.getMessage());
        }

    }

    //logout a keycloak user 
    

    //reset keycloak user password
    public KeyCloakOperationResult resetUserPassword(User user) {

        try {
            UserResource userResource = getUserResource(user);
            CredentialRepresentation credentialRepresentation = createPasswordCredentials(user.getPassword());
            userResource.resetPassword(credentialRepresentation);
            return KeyCloakOperationResult.Success;
        } catch (javax.ws.rs.NotFoundException e) {
            return KeyCloakOperationResult.ErrorUserNotFound;
        } catch (Exception e) {
            LogError("Error occured during keycloak user password reset", e);
            return KeyCloakOperationResult.ErrorSystemException.setMessage(e.getMessage());
        }
    }
    
    
    private UserResource getUserResource(User user) {
        UserResource userResource = KeyCloakRealmManager.getClientRealm().users()
                .get(user.getUserID());
        return userResource;
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }

}
