/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.iam.keycloak;

import static com.baobab.services.util.LogHandler.LogError;
import com.baobab.services.util.ServiceConfigs;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Optional;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.idm.KeysMetadataRepresentation.KeyMetadataRepresentation;

/**
 *
 * @author Eboda
 */
public class KeyCloakRealmManager {

    private static Keycloak keycloak = null;
    static PublicKey realmRSAKey;
    final static String serverUrl = (String) ServiceConfigs.getProperty("keycloak_serverUrl");// "http://localhost:8080/auth";
    final static String realm = (String) ServiceConfigs.getProperty("keycloak_realm");//"YOUR_REALM_NAME";
    final static String clientId = (String) ServiceConfigs.getProperty("keycloak_clientId");//"YOUR_CLIENT_ID";
    final static String clientSecret = (String) ServiceConfigs.getProperty("keycloak_clientSecret");//"YOUR_CLIENT_SECRET_KEY";
    final static String userName = (String) ServiceConfigs.getProperty("keycloak_userName");//"YOUR_REALM_ADMIN_USERNAME";
    final static String password = (String) ServiceConfigs.getProperty("keycloak_password");//"Your_REALM_ADMIN_PASSWORD";

    public static void initialize() {
        keycloak = KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .username(userName)
                .password(password)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .resteasyClient(new ResteasyClientBuilder()
                        .connectionPoolSize(10)
                        .build()
                ).build();

        Optional<KeyMetadataRepresentation> key = KeyCloakRealmManager.getInstance().realm(KeyCloakRealmManager.realm).keys().getKeyMetadata().getKeys().stream()
                .filter(k -> k.getType().equalsIgnoreCase("RSA") && k.getStatus().equalsIgnoreCase("ACTIVE")).findFirst();
        if (key.isPresent()) {
            KeyMetadataRepresentation rsaKey = key.get();
            System.out.println(rsaKey.getPublicKey());
            byte[] encodedPublicKey = java.util.Base64.getDecoder().decode(rsaKey.getPublicKey());
            X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
            try {
                KeyFactory kf = KeyFactory.getInstance("RSA");
                System.out.println(realmRSAKey = kf.generatePublic(spec));
            } catch (Exception e) {
                LogError("Unable to load RSA public key for realm ", e);
            }

        }
        //forEach((k)-> System.out.println(k.getType()));
    }

    public static Keycloak getInstance() {

        return keycloak;
    }

    public static RealmResource getClientRealm() {
        return keycloak.realm(KeyCloakRealmManager.realm);
    }

    public static TokenManager getTokenManager(String username, String password) {
        Keycloak kc = KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(KeyCloakRealmManager.realm)
                .grantType(OAuth2Constants.PASSWORD)
                .username(username)
                .password(password)
                .clientId(KeyCloakRealmManager.clientId)
                .clientSecret(KeyCloakRealmManager.clientSecret).build();
        return kc.tokenManager();

    }

}
