/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.iam.keycloak;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import io.jsonwebtoken.UnsupportedJwtException;
import java.security.Key;

/**
 *
 * @author WARI
 */
public class JWTUtils extends SigningKeyResolverAdapter {

    private static OpenIDConnectKeyResolver resolver = new OpenIDConnectKeyResolver();

    public static void verifyToken(String jwt) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException {
        Jws<Claims> claimsJwt = Jwts.parser().setSigningKeyResolver(resolver).parseClaimsJws(jwt);

    }

    private static class OpenIDConnectKeyResolver extends SigningKeyResolverAdapter {

        @Override
        public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {
            return KeyCloakRealmManager.realmRSAKey;
        }
    }
}
