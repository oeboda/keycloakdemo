/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.util;

import static com.baobab.services.util.LogHandler.LogError;
import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author Eboda
 */

public class ServiceConfigs {

    

    private static final Properties SERVICEPROPS = new Properties();

    static {

        try {
            String configFileName = System.getenv("SERVICE_CONFIG");
            if (configFileName == null) {
                configFileName = "service_config.properties";
            }

            try (FileInputStream in = new FileInputStream(configFileName)) {
                SERVICEPROPS.load(in);
            }
        } catch (Exception e) {
            LogError("Error loading configuration", e);
            System.exit(1);
        }

        System.getenv().forEach((k, v) -> {
            SERVICEPROPS.put(k, v);
        });

    }

    public static Object getProperty(String key) {
        return SERVICEPROPS.get(key);
    }

}
