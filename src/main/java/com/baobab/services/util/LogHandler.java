/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baobab.services.util;

import org.apache.logging.log4j.Logger;

/**
 *
 * @author Eboda
 */
public class LogHandler {

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(com.baobab.services.util.LogHandler.class);

    public static void LogError(String msg, Throwable e) {
        LOGGER.error(msg, e);
    }

    public static void LogInfo(String msg) {
        LOGGER.info(msg);
    }

}
